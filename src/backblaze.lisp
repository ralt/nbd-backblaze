(uiop:define-package :nbd-backblaze/src/backblaze
    (:use :cl)
  (:import-from :alexandria #:assoc-value)
  (:import-from :cl-json #:decode-json-from-string #:encode-json-plist-to-string)
  (:import-from :drakma
                #:http-request
                #:*drakma-default-external-format*
                #:*text-content-types*)
  (:import-from :flexi-streams #:flexi-stream-stream)
  (:import-from :local-time #:now #:timestamp< #:timestamp-)
  (:import-from :quri #:merge-uris #:render-uri #:uri)
  (:import-from :uiop #:strcat)
  (:export #:make-bb-session
           #:upload
           #:with-download-stream))

(in-package :nbd-backblaze/src/backblaze)

(defvar *api-url* (uri "https://api.backblazeb2.com/b2api/v2/"))

(push (cons "application" "json") drakma:*text-content-types*)
(setf drakma:*drakma-default-external-format* :utf8)

(defun merge-uris* (base &rest references)
  (let ((reference (first references)))
    (if reference
        (apply #'merge-uris* (append (list (merge-uris reference base))
                                     (rest references)))
        base)))

(defclass bb-session ()
  ((authorization-token :initarg :authorization-token :accessor authorization-token)
   (upload-auth-token :initarg :upload-auth-token :accessor upload-auth-token)
   (last-token-renewal :initarg :last-token-renewal :accessor last-token-renewal)
   (api-url :initarg :api-url :reader api-url)
   (download-url :initarg :download-url :reader download-url)
   (upload-url :initarg :upload-url :accessor upload-url)
   (account-id :initarg :account-id :reader account-id)
   (key-id :initarg :key-id :reader key-id)
   (application-key :initarg :application-key :reader application-key)
   (bucket-name :initarg :bucket-name :reader bucket-name)
   (bucket-id :initarg :bucket-id :reader bucket-id)))

(defun make-bb-session (key-id application-key bucket-name)
  "Returns a backblaze session that manages its authorization cycle."
  (let* ((authorization (decode-json-from-string
                         (http-request (render-uri
                                        (merge-uris "b2_authorize_account" *api-url*))
                                       :basic-authorization (list key-id application-key))))
         (allowed (assoc-value authorization :allowed))
         (api-url (merge-uris "/b2api/v2/" (assoc-value authorization :api-url))))
    (unless (string= bucket-name (assoc-value allowed :bucket-name))
      (error "The token must be restricted to the bucket name."))
    (let* ((authorization-token (assoc-value authorization :authorization-token))
           (bucket-id (assoc-value allowed :bucket-id))
           (upload-authorization
             (decode-json-from-string
              (http-request
               (render-uri (merge-uris "b2_get_upload_url" api-url))
               :method :post
               :additional-headers `(("Authorization" . ,authorization-token))
               :content (encode-json-plist-to-string `(:bucket-id ,bucket-id))))))
      (make-instance 'bb-session
                     :authorization-token authorization-token
                     :upload-auth-token (assoc-value upload-authorization
                                                     :authorization-token)
                     :last-token-renewal (now)
                     :api-url api-url
                     :download-url (uri (assoc-value authorization :download-url))
                     :upload-url (uri (assoc-value upload-authorization :upload-url))
                     :account-id (assoc-value authorization :account-id)
                     :key-id key-id
                     :application-key application-key
                     :bucket-name bucket-name
                     :bucket-id bucket-id))))

(defmethod maybe-renew-token ((b bb-session))
  (when (timestamp< (last-token-renewal b) (timestamp- (now) 12 :hour))
    (let* ((authorization (decode-json-from-string
                           (http-request (render-uri
                                          (merge-uris "b2_authorize_account" *api-url*))
                                         :basic-authorization (list
                                                               (key-id b)
                                                               (application-key b)))))
           (authorization-token (assoc-value authorization :authorization-token))
           (upload-authorization
             (decode-json-from-string
              (http-request
               (render-uri (merge-uris "b2_get_upload_url" (api-url b)))
               :method :post
               :additional-headers `(("Authorization" . ,authorization-token))
               :content (encode-json-plist-to-string `(:bucket-id ,(bucket-id b)))))))
      (setf (authorization-token b) (assoc-value authorization :authorization-token))
      (setf (upload-auth-token b) (assoc-value upload-authorization :authorization-token))
      (setf (upload-url b) (assoc-value upload-authorization :upload-url))
      (setf (last-token-renewal b) (now)))))

(defmethod download ((b bb-session) file-name)
  (maybe-renew-token b)
  (http-request
   (render-uri (merge-uris* (download-url b)
                            "/file/"
                            (strcat (bucket-name b) "/")
                            file-name))
   :additional-headers `(("Authorization" . ,(authorization-token b)))
   :want-stream t))

(defmacro with-download-stream ((stream) (bb-session file-name) &body body)
  "Get a binary stream to download a file. Usage is as such:

(with-download-stream (stream) (session \"file-name\")
  (read-byte stream))

The stream is automatically closed at the end.
"
  `(let ((,stream (flexi-stream-stream (download ,bb-session ,file-name))))
     (unwind-protect
          (progn ,@body)
       (close ,stream))))

(defmethod upload ((b bb-session) checksum file-name content-length stream)
  "Upload STREAM to FILE-NAME on bb.

CHECKSUM can be either :none or :end. If :none, then BB will not
verify the hash of your upload. If :end, you must append a 40-char
sha1 hash of the uploaded file, so that BB can verify it with what it
got. CONTENT-LENGTH must be the length of STREAM, without the hash."
  (let ((checksump (ecase checksum
                     (:none nil)
                     (:end t))))
    (maybe-renew-token b)
    (http-request
     (render-uri (upload-url b))
     :method :post
     :content stream
     :content-length (if checksump (+ 40 content-length) content-length)
     :content-type "b2/x-auto"
     :additional-headers `(("Authorization" . ,(upload-auth-token b))
                           ("x-bz-file-name" . ,file-name)
                           ("x-bz-content-sha1" . ,(if checksump
                                                       "hex_digits_at_end"
                                                       "do_not_verify"))))))
