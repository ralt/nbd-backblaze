(uiop:define-package :nbd-backblaze/src/main
    (:use :cl :nbd/api)
  (:import-from :cacle #:make-cache #:cache-fetch)
  (:import-from :cl-cpus #:get-number-of-processors)
  (:import-from :flexi-streams #:with-input-from-sequence)
  (:import-from :sb-concurrency #:make-mailbox #:receive-message #:send-message)
  (:import-from :sb-thread #:make-thread #:make-mutex #:with-mutex)
  (:import-from :uiop #:xdg-runtime-dir)
  (:export #:main))

(in-package :nbd-backblaze/src/main)

(defvar *request->workers* (make-mailbox :name "request->workers mailbox"))
(defvar *workers->writer* (make-mailbox :name "workers->write mailbox"))

(defvar *workers* nil)
(defvar *writer* nil)

(defvar *stream-mutex* (make-mutex :name "Stream mutex")
  "A worker needs to hold this mutex while writing to the response stream.")

(defmacro with-stream ((stream) &body body)
  `(with-mutex (*stream-mutex*)
     (progn ,@body)
     (finish-output ,stream)))

(defvar *data-cache* nil)

(defun main ()
  (let* ((export-name "bucket")
         (export-size (* 50 1024 1024))
         (num-cpus (get-number-of-processors))
         (provider (make-instance 'data-provider
                                  :export-name export-name
                                  :export-size export-size)))
    (dotimes (i (* 2 num-cpus))
      (push (make-thread #'worker
                         :name (format nil "Worker ~a" i)
                         :arguments (list provider))
            *workers*))
    (setf *writer* (make-thread #'writer :name "Writer" :arguments (list provider)))
    (wait (start #'on-request
                 (xdg-runtime-dir "nbd-backblaze.sock")
                 export-name export-size
                 (list *send-flush*)
                 export-name 100 1 4096
                 ;; Given that we fetch all the data before writing it
                 ;; to the socket, let's make sure we don't get
                 ;; unbounded lengths from the client. We have to
                 ;; fetch all of it because nbd-client doesn't support
                 ;; structured replies, which means that if we don't
                 ;; want to take the stream lock for too long, we have
                 ;; to fetch all ahead of time to write all at once
                 ;; the reply.
                 65535))))

(defun on-request (type flags handle offset length stream start-reply)
  (send-message
   *request->workers*
   (list request-mailbox type flags handle offset length stream start-reply)))

(defclass data-provider ()
  ((export-name :initarg :export-name :reader export-name)
   (export-size :initarg :export-size :reader export-size)
   (cache :accessor cache)))

(defmethod initialize-instance ((provider data-provider) &key)
  (setf (cache provider) (make-cache 100000 ; 4096*100000 = 400MB
                                     (cache-provider provider)
                                     :policy :lru
                                     :test #'equal)))

(defclass data-block ()
  ((mutex :initarg :mutex :reader mutex)
   (data :initarg :data :reader data)))

(defmethod cache-provider ((provider data-provider))
  (let ()
    (lambda (key)
      (destructuring-bind (begin end) key
        ;; todo: read from BB
        (values (make-instance 'data-block
                               :mutex (make-mutex)
                               :data (make-array 4096 :element-type '(unsigned-byte 8)))
                1)))))

(defun worker (data-provider)
  (loop
    (destructuring-bind (type flags handle offset length stream start-reply)
        (receive-message *request->workers*)
      (declare (ignore flags handle))
      (case type
        (:read
         ;; The length is max 65k, memory is not unbounded.
         (let ((blocks (find-blocks data-provider offset length)))
           (with-stream (stream)
             (funcall start-reply)
             (dolist (b blocks)
               (with-mutex ((mutex b))
                 (write-sequence (data b) stream))))))
        (:write
         (let ((buffer (make-array length :element-type '(unsigned-byte 8))))
           (read-sequence buffer stream)
           (send-message *workers->writer* (list :write buffer offset length))
           (let ((blocks (find-blocks data-provider offset length)))
             (with-input-from-sequence (s buffer)
               (dolist (b blocks)
                 (with-mutex ((mutex b))
                   (read-sequence (data b) s)))))
           (with-stream (stream)
             (funcall start-reply))))
        (:flush
         (let ((queue (make-mailbox)))
           (send-message *workers->writer* (list :wait queue))
           (receive-message queue)
           (with-stream (stream)
             (funcall start-reply))))))))

(defun writer (data-provider)
  (loop
    (let ((message (receive-message *workers->writer*)))
      (case (first message)
        (:wait
         (send-message (second message) nil))
        (:write
         (let ((buffer (second message))
               (offset (third message))
               (length (fourth message)))
           ;; todo: write to BB
           ))))))

(defun find-ranges (offset length)
  "Do the very complicated math to figure out the 4096 marks without
  off-by-one errors. Big challenge.")

(defun find-blocks (data-provider offset length)
  (mapcar
   (lambda (range)
     ;; cut down at beginning and end?
     (cache-fetch (cache data-provider) range))
   (find-ranges offset length)))
