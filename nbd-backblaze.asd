(defsystem "nbd-backblaze"
  :author "Florian Margaine <florian@margaine.com>"
  :description "NBD backed by BackBlaze"
  :license "GPLv2"
  :defsystem-depends-on ("wild-package-inferred-system")
  :class "winfer:wild-package-inferred-system"
  :depends-on ("nbd-backblaze/src/*"))

(defsystem "nbd-backblaze/rpm"
  :author "Florian Margaine <florian@margaine.com>"
  :description "NBD backed by BackBlaze"
  :license "GPLv2"
  :defsystem-depends-on ("linux-packaging")
  :depends-on ("nbd-backblaze")
  :class "linux-packaging:rpm"
  :package-name "nbd-backblaze"
  :build-operation "linux-packaging:build-op"
  :build-pathname "nbd-bacbklaze"
  :entry-point "nbd-backblaze/src/main:main")
